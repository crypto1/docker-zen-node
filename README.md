# Docker Zencash Secure Node

I cloned the original repository from https://github.com/WhenLamboMoon/docker-zen-node to gitlab in order to
* set up automatic builds
* switch from hub.docker.com to registry.gitlab.com (check out what is coming for docker hub users: https://www.docker.com/blog/docker-hub-image-retention-policy-delayed-and-subscription-updates/)
* do further customizations to improve the maintenance process

## Improvements

DONE
- #1: build zend docker image automatically and publish to gitlab container registry (https://gitlab.com/crypto1/docker-zen-node/container_registry/)
```
docker pull registry.gitlab.com/crypto1/docker-zen-node/zend:latest
```

TODO
- build nodetracker docker image automatically and publish to gitlab container registry
- build zend automatically (once a day) when a new version is released

NOTES

```bash
# test a new release build locally
docker build --build-arg IMAGE_NAME=ubuntu:22.04 --build-arg ZEND_VERSION=5.0.0 -t zend:tmp22 zend/

# for node tracker
docker build --build-arg IMAGE_NAME=node:20.11.0 --build-arg NODETRACKER_VERSION=0.5.1 -t nodetracker:tmp20 secnodetracker/

```

## Update one node manually

```bash
# upgrade 1 node
# to ubuntu-22.04-v4.1.1

docker pull registry.gitlab.com/crypto1/docker-zen-node/zend:ubuntu-22.04-v5.0.0
sudo sed -i -r 's/^(ExecStart=)(.+)(zend:.+)$/\1\2zend:ubuntu-22.04-v5.0.0/' /etc/systemd/system/zen-node.service

cat /etc/systemd/system/zen-node.service
sudo systemctl daemon-reload
sudo systemctl restart zen-node
docker exec zen-node gosu user zen-cli getinfo | grep \"version
sudo systemctl restart zen-secnodetracker
docker logs -f zen-secnodetracker


# upgrade nodetracker
docker pull registry.gitlab.com/crypto1/docker-zen-node/nodetracker:node-20.11.0-v0.5.1-update-node-tracker-84d54717

# sudo vim /etc/systemd/system/zen-secnodetracker.service
# sudo sed -i -r 's/^(ExecStart=)(.+)(nodetracker:.+)$/\1\2nodetracker:node-20.11.0-v0.5.1-update-node-tracker-84d54717/' /etc/systemd/system/zen-secnodetracker.service

cat /etc/systemd/system/zen-secnodetracker.service
sudo systemctl daemon-reload
sudo systemctl restart zen-secnodetracker
docker logs -f zen-secnodetracker

```

[The original readme file](README_ORG.md)
